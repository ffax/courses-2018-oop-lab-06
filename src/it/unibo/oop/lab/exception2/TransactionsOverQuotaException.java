package it.unibo.oop.lab.exception2;

public class TransactionsOverQuotaException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String getMessage() {
		return "Transactions Over Quota from ATM";
	}
	

}
