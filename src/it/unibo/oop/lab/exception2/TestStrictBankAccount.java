package it.unibo.oop.lab.exception2;

import static org.junit.Assert.fail;

import org.junit.Test;

/**
 * JUnit test to test
 * {@link StrictBankAccount}.
 * 
 */
public class TestStrictBankAccount {

    /**
     * Used to test Exceptions on {@link StrictBankAccount}.
     */
    @Test
    public void testBankOperations() {
        /*
         * 1) Creare due StrictBankAccountImpl assegnati a due AccountHolder a
         * scelta, con ammontare iniziale pari a 10000 e nMaxATMTransactions=10
         * 
         * 2) Effetture un numero di operazioni a piacere per verificare che le
         * eccezioni create vengano lanciate soltanto quando opportuno, cioè in
         * presenza di un id utente errato, oppure al superamento del numero di
         * operazioni ATM gratuite.
         */
    	AccountHolder usr1 = new AccountHolder("Mario", "Rossi", 1);
    	BankAccount acc1 = new StrictBankAccount(usr1.getUserID(), 10000, 10);
    	AccountHolder usr2 = new AccountHolder("Franco", "Bianchi", 2);
    	BankAccount acc2 = new StrictBankAccount(usr2.getUserID(), 10000, 10);
    	
    	try {
    		acc1.withdraw(usr2.getUserID(), 100);
    		fail();
    	}
    	catch (TransactionsOverQuotaException t) {
    		fail("Not expected Transaction Exception");
    	}
    	catch (NotEnoughFoundsException n) {
    		fail("Not expected Not Money Exception");
    	}
    	catch (WrongAccountHolderException w) {
    		System.out.println(w.getMessage());
    	}
    	
    	try {
    		acc1.withdraw(usr1.getUserID(), 10001);
    		fail();
    	}
    	catch (TransactionsOverQuotaException t) {
    		fail("Not expected Transaction Exception");
    	}
    	catch (WrongAccountHolderException w) {
    		fail("Not expected Wrong accountexception");
    	}
    	catch (NotEnoughFoundsException n) {
    		System.out.println(n.getMessage());
    	}
    	
    	try {
    		for (int i = 0; i<10;i++) {
    			acc2.withdrawFromATM(usr2.getUserID(),23);
    		}
    		
    		acc2.withdrawFromATM(usr2.getUserID(), 278);
    		fail("hai fallito");
    	}
    	catch (WrongAccountHolderException w) {
    		fail("Not expected Wrong accountexception");
    	}
    	catch (NotEnoughFoundsException n) {
    		fail("Not expected Not enough money Exception");;
    	}
     	catch (TransactionsOverQuotaException t) {
    		System.out.println(t.getMessage());
    	}
    	
    }
}
