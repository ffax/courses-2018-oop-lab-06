package it.unibo.oop.lab.exception2;

public class NotEnoughFoundsException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String getMessage() {
		return "Not Enough Founds";
	}

	
}
