package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Example class using {@link List} and {@link Map}.
 * 
 */
public final class UseCollection {

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	ArrayList<Integer> arr = new ArrayList<>();
    	for (int i = 1000; i < 2000; i++) {
    		arr.add(i);
    	}
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	LinkedList<Integer> list = new LinkedList<>(arr);
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	arr.set(arr.size()-1, arr.set(0, arr.get(arr.size()-1)));
    	System.out.println(arr);
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	for (final int el : arr) {
    		System.out.print(el + " ");
    				
    	}System.out.println(); 
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
    	long time;
    	time = System.nanoTime();
    	for (int i = 0; i < 100000; i++) {
    		arr.add(0,i);
    	}
    	time = System.nanoTime() - time;
    	System.out.println("Time to add 100000 el in the head of a arrayList:  " + time);
    	
    	time = System.nanoTime();
    	for (int i = 0; i < 100000; i++) {
    		list.add(0,i);
    	}
    	time = System.nanoTime() - time;
    	System.out.println("Time to add 100000 el in the head of a linkedList: " + time);
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
    	time = System.nanoTime();
    	for (int i = 0; i < 1000; i++) {
    		arr.get(arr.size()/2);
    	}
    	time = System.nanoTime() - time;
    	System.out.println("Time to get 1000 el in the middle of a arrayList:  " + time);
    	
    	time = System.nanoTime();
    	for (int i = 0; i < 1000; i++) {
    		list.get(list.size()/2);
    	}
    	time = System.nanoTime() - time;
    	System.out.println("Time to get 1000 el in the middle of a linkedList: " + time);
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:         
         */
    	Map<String,Long> world = new HashMap<>();
    	world.put("Africa", 1110635000l);
    	world.put("Americas", 972005000l);
    	world.put("Antarctica", 0l);
    	world.put("Asia",4298723000l);
    	world.put("Europe", 742452000l);
    	world.put("Oceania", 38304000l);
    	
    	long tot = 0;
    	System.out.println("world population: ");
    	for (final Long parz : world.values()) {
    		tot += parz; 
    	}
    	System.out.println(tot);
        /*
         * 8) Compute the population of the world
         */
    }
}
